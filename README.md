# BR StatMilk Coding Exercise Solution

## Requirements

Redis

Redis host and port to be set in configuration file appsettings.json.

```json
"Redis": { "HostWithPort": "localhost:6379" }
```

## Solution

* Enabled distributed cache for redis in the Startup class
* Modified the interface file IDataProvider.cs as follows
```c#
//fetch data from cache or remote endpoint
Task<DataResponseModel> FetchPlayersData();

//get player
Task<PlayerModel> GetPlayerById(string id);

//get latest on player
Task<LastestDateModel> GetLatest(string id);
```	
* Modified DataProviderImpl.cs class to implement the above interfaces.

* By default data is fetched from cache, when the data is not found or expired, then a request is made to the remote endpoint. 
  The data fetched is then stored in cache so on future request the data is retrieved from cache.

* Moved the remote endpoint url into configuration file appsettings.json.

* Modified the actions in the controller class to produce the desired results.

* Fixed broken links in the /home/index view.
