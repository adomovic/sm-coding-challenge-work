using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace sm_coding_challenge.Models
{
    [DataContract]
    public class PlayerModel
    {
        [DataMember(Name = "player_id")]
        public string Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "position")]
        public string Position { get; set; }

        //attributes
        [DataMember(Name = "yds")]
        public string Yds { get; set; }

        [DataMember(Name = "att")]
        public string Att { get; set; }

        [DataMember(Name = "tds")]
        public string Tds { get; set; }

        [DataMember(Name = "fum")]
        public string Fum { get; set; }

        [DataMember(Name = "cmp")]
        public string Cmp { get; set; }

        [DataMember(Name = "int")]
        public string Int { get; set; }

        [DataMember(Name = "rec")]
        public string Rec { get; set; }

        [DataMember(Name = "fld_goals_made")]
        public string FieldGoals { get; set; }

        [DataMember(Name = "fld_goals_att")]
        public string FieldGoalsAtt { get; set; }

        [DataMember(Name = "extra_pt_made")]
        public string ExtraPtsMade { get; set; }

        [DataMember(Name = "extra_pt_att")]
        public string ExtraPtsAtt { get; set; }
    }
    
}

