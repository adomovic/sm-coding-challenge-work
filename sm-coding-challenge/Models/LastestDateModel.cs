using System.Collections.Generic;
using System.Runtime.Serialization;

namespace sm_coding_challenge.Models
{
    public class LastestDateModel
    {
        public List<PlayerModel> Rushing { get; set; }

        public List<PlayerModel> Passing { get; set; }

        public List<PlayerModel> Receiving { get; set; }

        public List<PlayerModel> Kicking { get; set; }

        public LastestDateModel()
        {

        }
    }
}

