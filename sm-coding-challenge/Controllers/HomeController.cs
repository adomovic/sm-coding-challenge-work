﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using sm_coding_challenge.Models;
using sm_coding_challenge.Services.DataProvider;

namespace sm_coding_challenge.Controllers
{
    public class HomeController : Controller
    {

        private readonly IDataProvider _dataProvider;
        public HomeController(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider ?? throw new ArgumentNullException(nameof(dataProvider));
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Player(string id) //made the action async as the data provider has been made async
        {
            var player = await _dataProvider.GetPlayerById(id);

            //show a user friendly message when no records are found
            return player == null ? 
                Json(new { Content = "No player found" }) : 
                Json(player);
        }

        [HttpGet]
        public async Task<IActionResult> Players(string ids) //made the action async as the data provider has been made async
        {
            var idList = ids.Split(',');
            var returnList = new List<PlayerModel>();
            foreach (var id in idList)
            {
                var player = await _dataProvider.GetPlayerById(id);
                if(player == null) continue;
                
                returnList.Add(player);
            }

            //show a user friendly message when no records are found
            return !returnList.Any() ? 
                Json(new {Content = "No players found"}) : 
                Json(returnList.GroupBy(c => c.Id).Select(c => c.First())); // show distinct values based on id
        }

        [HttpGet]
        public async Task<IActionResult> LatestPlayers(string ids) //made the action async as the data provider has been made async
        {
            var latest = await _dataProvider.GetLatest(ids);
            return latest == null ? Json(new {Content = "No latest content found"}) : Json(latest);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
