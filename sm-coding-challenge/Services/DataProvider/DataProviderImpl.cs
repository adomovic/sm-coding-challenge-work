using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using sm_coding_challenge.Models;

namespace sm_coding_challenge.Services.DataProvider
{
    public class DataProviderImpl : IDataProvider
    {
        public TimeSpan Timeout = TimeSpan.FromSeconds(30); //remove the static keyword as this is not a static class
        private readonly IConfiguration _configuration;
        private readonly IDistributedCache _cache;
        private string _cacheKey = "nfldata";
        private readonly ILogger<DataProviderImpl> _logger; //added logging to log errors

        public DataProviderImpl(IConfiguration configuration, 
            IDistributedCache cache, ILogger<DataProviderImpl> logger)
        {
            _configuration = configuration;
            _cache = cache;
            _logger = logger;
        }

        //created a function to firstly fetch from cache,
        //if cache is null or expired then it should fetch from remote endpoint
        public async Task<DataResponseModel> FetchPlayersData()
        {
            DataResponseModel playerData;

            ////fetch from cache
            var cachedData = await _cache.GetStringAsync(_cacheKey);

            if (cachedData != null)
            {
                //deserialize cache data
                playerData = JsonConvert.DeserializeObject<DataResponseModel>(cachedData);
            }
            else
            {
                try
                {
                    var handler = new HttpClientHandler()
                    {
                        AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                    };
                    using (var client = new HttpClient(handler))
                    {
                        client.Timeout = Timeout;

                        //pick the nfl data url from config file
                        var response = await client.GetAsync(_configuration["NflData:Url"]);

                        //return null when no record is found or error
                        if (!response.IsSuccessStatusCode)
                        {
                            var errorMsg = new
                            {
                                response.StatusCode,
                                Reason = response.ReasonPhrase,
                                RequestMessage = JsonConvert.SerializeObject(response.RequestMessage)
                            };

                            //log error when api request is not successful
                            _logger.LogError($"NFL Data Fetch Error: { JsonConvert.SerializeObject(errorMsg, Formatting.None)  }");
                            return null;
                        }

                        var stringData = await response.Content.ReadAsStringAsync();
                        var dataResponse = JsonConvert.DeserializeObject<DataResponseModel>(stringData, new JsonSerializerSettings
                        {
                            TypeNameHandling = TypeNameHandling.Auto
                        });

                        //load data into cache
                        await _cache.SetStringAsync(_cacheKey,
                            JsonConvert.SerializeObject(dataResponse),
                            new DistributedCacheEntryOptions
                            {
                                SlidingExpiration = TimeSpan.FromHours(1),
                                AbsoluteExpiration = DateTime.Now.AddHours(6)
                            });

                        playerData = dataResponse;
                    }
                }
                catch (Exception ex)
                {
                    //log error when api request is not successful
                    _logger.LogError(ex, $"NFL Data Fetch Exception");
                    return null;
                }
                
            }

            return playerData;
        }

        //modified the GetPlayerById
        public async Task<PlayerModel> GetPlayerById(string id)
        {
            var playersData = await FetchPlayersData();
            if (playersData == null) return null;

            var rushingPlayer = FindPlayer(id, playersData.Rushing);
            if (rushingPlayer != null) return rushingPlayer;

            var passingPlayer = FindPlayer(id, playersData.Passing);
            if (passingPlayer != null) return passingPlayer;

            var receivingPlayer = FindPlayer(id, playersData.Receiving);
            if (receivingPlayer != null) return receivingPlayer;

            var kickingPlayer = FindPlayer(id, playersData.Kicking);
            if (kickingPlayer != null) return kickingPlayer;

            return null;
        }

        //get the latest
        public async Task<LastestDateModel> GetLatest(string id)
        {
            var playersData = await FetchPlayersData();
            if (playersData == null) return null;

            var latestData = new LastestDateModel
            {
                Rushing = FindPlayers(id, playersData.Rushing),
                Receiving = FindPlayers(id, playersData.Receiving)
            };

            return latestData;
        }

        //a simple private finder method to be used by GetPlayerById
        PlayerModel FindPlayer(string id, IEnumerable<PlayerModel> players)
        {
            return players.FirstOrDefault(c => string.Equals(c.Id, id, StringComparison.OrdinalIgnoreCase));
        }

        List<PlayerModel> FindPlayers(string id, IEnumerable<PlayerModel> players)
        {
            return players.Where(c => string.Equals(c.Id, id, StringComparison.OrdinalIgnoreCase)).ToList();
        }

    }
}
