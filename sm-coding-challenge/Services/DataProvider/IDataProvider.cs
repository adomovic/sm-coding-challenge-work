using System.Collections.Generic;
using System.Threading.Tasks;
using sm_coding_challenge.Models;

namespace sm_coding_challenge.Services.DataProvider
{
    public interface IDataProvider
    {
        //fetch data from cache or remote endpoint
        Task<DataResponseModel> FetchPlayersData();

        //get player
        Task<PlayerModel> GetPlayerById(string id);

        //get latest on player
        Task<LastestDateModel> GetLatest(string id);
    }
}
